clear;
slider = Slider();

try
    % Used for plots
    times = [];
    positions = [];
    actualPositions = [];
    velocities = [];
    visualAngles = [];

    delay = 0.20; % Units: seconds
    time = 0; % Units: seconds

    % Computed position based on parametric
    position = getPosition(0);

    startTic = tic();

    while time < 40.0
        fprintf('time: %f\n', time);
        fprintf('position: %f\n', position);

        % This line is above the others because it takes a while to execute and
        % may affect timing
        actualPositions = [actualPositions, slider.currentPosition()];

        time = toc(startTic);
        nextTime = time + delay;
        nextPosition = getPosition(nextTime);
        velocity = (nextPosition - position) / delay;

        fprintf('nextTime: %f\n', nextTime);
        fprintf('nextPosition: %f\n', nextPosition);
        fprintf('velocity: %f\n', velocity);

        times = [times, time];
        positions = [positions, position];
        visualAngles = [visualAngles, getVisualAngle(nextTime)];
        velocities = [velocities, velocity];

        % We don't do `slider.moveTo(nextPosition)` because that will cause the
        % slider to stop if it gets there early. This makes the movement jerky.
        % Instead we tell the slider to run at the desired speed and hope that
        % it will be at `nextPosition` by the next iteration.
        %
        % `velocityCoefficient` was determined through the trial and error and
        % is vital in ensuring the proper timing described above.
        velocityCoefficient = 0.91
        slider.setSpeed(velocity * velocityCoefficient);

        pause(delay);

        position = nextPosition;
        time = nextTime;
    end
catch ME
    fprintf('Unexpected error, closing connection to slider.\n');
    preDelete(slider);
    slider.delete();
    clear slider;

    rethrow(ME)
end

preDelete(slider);
slider.delete();
clear slider;

plot(times, positions, '-o');
hold on;

% Must offset with `getPosition(0)` because the always slider thinks that it
% starts at zero
plot(times, actualPositions + getPosition(0), '-o');

plot(times, velocities, '-o');
hold off;

title('Slider movement');
xlabel('Time [s]');
legend({'Position', 'Actual Position', 'Velocity'});
grid on;

figure();
plot(times, visualAngles, '-o');
title('Visual angle');

function visualAngle = getVisualAngle(time)
    % Produces a sinusoidal output
    amplitude = 2; % Units: cpd
    initialValue = 8; % Units: cpd
    period = 20.0; % Units: seconds
    angularFreq = (1 / period) * 2 * pi;
    visualAngle = -1 * amplitude * cos(angularFreq * time) + initialValue;
end

function position = getPosition(time)
    % Uses `getVisualAngle` to calculate the slider position (i.e. target
    % distance). This is derived from simple trigonometry.
    interpupillaryDistance = 6 * stepsPerCm; % Units: steps
    visualAngle = getVisualAngle(time);
    position = 0.5 * interpupillaryDistance / tand(0.5 * visualAngle);
end

function preDelete(slider)
    slider.setSpeed(0);
end
