clear;

slider = Slider();

% Double the amplitude of the sine wave
distance = 1000;

% Values higher than 400 causes the slider to stutter
slider.setMaxSpeed(350);
slider.setAcceleration(100);

% Accelerate until max speed is reached, then decelerate as target position is
% reached. This is approximately sinusoidal.
slider.moveTo(distance);

% Count the number of back and forth trips (oscillations)
trips = 0;

ts = []; % Times (used for plot)
positions = []; % Positions (used for plot)

% Record the elapsed time using MATLAB's `tic` and `toc` functions
startTic = tic();

% 1 = forward, 0 = backward
direction = 1;

while trips < 4
    if slider.distanceToGo() == 0
        if direction == 1
            slider.moveTo(0);
            direction = 0;
        else
            slider.moveTo(distance);
            direction = 1;
        end

        trips = trips + 1;
        fprintf('current pos: %i\n', slider.currentPosition())
    end

    % Record data for plots
    ts = [ts, toc(startTic)];
    positions = [positions, slider.currentPosition()];
end

slider.moveTo(0);

% Wait until the the slider has arrived at `targetPos`
while ~slider.hasArrived()
end

% This must be called. Even if the motor has reached the target position, if
% the speed is not zero, the Arduino will still continue to send current to the
% stepper. This will tell the Arduino to stop doing so.
slider.setSpeed(0);

% Close the connection to the Arduino
slider.delete();
clear slider;

plot(ts, positions, '-o');
title('Slider positions over time');
xlabel('Time [s]');
ylabel('Position [steps]');
